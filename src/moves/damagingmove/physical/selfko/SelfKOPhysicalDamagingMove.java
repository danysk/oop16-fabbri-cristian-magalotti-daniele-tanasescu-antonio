package moves.damagingmove.physical.selfko;

import java.util.Random;

import abilities.otherconditions.Damp;
import abilities.otherconditions.Scrappy;
import battle_arena.BattleArena;
import battle_arena.terrain.PsychicTerrain;
import battle_arena.terrain.Terrain;
import main.view.BattleMenuController;
import moves.damagingmove.physical.PhysicalDamagingMove;
import pokemon.Pokemon;
import types.Fight;
import types.Ghost;
import types.Normal;
import types.Type;

public abstract class SelfKOPhysicalDamagingMove extends PhysicalDamagingMove{

    public SelfKOPhysicalDamagingMove(String name, String description, int moveBasePower, Type moveType, double moveAccuracy,
            double actualCritRange, int minPP, int priority) {
        super(name, description, moveBasePower, moveType, moveAccuracy, actualCritRange, minPP, priority);
    }

    @Override
    public void getDamage(Pokemon user, Pokemon target, BattleArena battleArena){
        BattleMenuController.battleLogManager.setUsedMoveMessage(user, target, battleArena, this);
        Damp damp = new Damp();
        if(user.getAbility().equals(damp)){
            ((Damp)user.getAbility()).activateAbility(user, target, battleArena);
        }
        else if(target.getAbility().equals(damp)){
            ((Damp)target.getAbility()).activateAbility(user, target, battleArena);
        }
        else{
            this.setHasRecoil(true);                                          //it's the recoil part
            user.takeDamage(user.getMaxHp(), this.hasRecoil());               //user faints
            this.setHasRecoil(false);                                         //it's the damage part
            this.applicateDamage(user, target, battleArena);                  //can't use the super one because it woudl display the attack twice

        }
    }

    private void applicateDamage(Pokemon user, Pokemon target, BattleArena battleArena){
        if(!target.isFainted()){
            if(!this.hasMoveFailed(user, target, battleArena)){
                if(!target.isProtecting){
                    user.isAttacking = true;
                    user.lastMoveUsed = this;
                    double damage = ((double)(((2d*user.getLevel()/5d)+2) * user.getAtk() * this.getMoveBasePower()))/(50 * target.getDef())+2;

                    for(Type type : target.getType()){
                        if(type != null){
                            if(Type.containsType(type.getTypeImmunities(), this.getMoveType())){
                                //if has scrappy ability with right conditions, it will attack just the same
                                if(! (type.equals(new Ghost()) && 
                                        (this.getMoveType().equals(new Normal()) || this.getMoveType().equals(new Fight())) &&
                                        user.getAbility().equals(new Scrappy()))
                                        ){
                                    user.effectiveness *= 0;
                                    BattleMenuController.battleLogManager.setEffectivenessMessage(user.effectiveness);
                                    return;
                                }
                            }
                            else if(Type.containsType(type.getTypeResistances(), this.getMoveType())){
                                user.effectiveness *= 0.5;
                            }
                            else if(Type.containsType(type.getTypeWeaknesses(), this.getMoveType())){
                                user.effectiveness *= 2;
                            }
                        }
                    }     
                    for(Type type : user.getType()){
                        if(this.getMoveType().equals(type)){
                            user.stab *= 1.5;
                        }
                    }

                    int standardMovePower = this.getMoveBasePower();                                    //keep initial move's power, 'cause it could change!

                    if(battleArena.weather!= null){
                        battleArena.weather.checkForWeatherPowerChange(this);                           //this can change move's power 
                    }

                    if(battleArena.terrain != null){
                        if(battleArena.terrain.equals(new PsychicTerrain(5)) && this.getPriority() > 0){
                            if(Terrain.doesPokemonGainEffect(target)){
                                battleArena.terrain.getTerrainPreventMoveMessage(target);
                                return;
                            }
                        }
                        else{
                            battleArena.terrain.getTerrainMovePowerChange(target, this);
                        }
                    }

                    if(this.critHit()){
                        user.crit *= 1.5;
                    }

                    Random randomRoll = new Random();
                    user.roll =  0.85 + randomRoll.nextInt(26)/100;

                    user.getAbility().checkForActivation(user, target, battleArena);
                    target.getAbility().checkForActivation(target, user, battleArena);

                    if(user.effectiveness > 0){                                                     //(also an ability could have changed it to 0)
                        BattleMenuController.battleLogManager.setEffectivenessMessage(user.effectiveness);
                    }

                    damage *= user.effectiveness * user.stab * user.crit * user.roll;

                    this.setLastDamageDone(damage);                  

                    if(damage > 0){
                        target.takeDamage(damage, this.hasRecoil());
                        if(user.crit > 1){
                            BattleMenuController.battleLogManager.setCritMessage();
                        }
                        if(this.sideEffect){
                            this.sideEffect(user, target, battleArena);
                        }
                    }

                    user.getAbility().checkForActivation(user, target, battleArena);
                    target.getAbility().checkForActivation(target, user, battleArena);

                    user.isAttacking = false;
                }
                else{
                    BattleMenuController.battleLogManager.setProtectMessage(target);

                }
            }   
            else{
                user.isAttacking = false;
                BattleMenuController.battleLogManager.setMoveFailedMassage();
            }
        }
        else{
            BattleMenuController.battleLogManager.setMoveFailedMassage();
        }
    }


}
